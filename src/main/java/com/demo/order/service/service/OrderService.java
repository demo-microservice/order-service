package com.demo.order.service.service;

import com.demo.order.service.common.Payment;
import com.demo.order.service.common.TransactionRequest;
import com.demo.order.service.common.TransactionResponse;
import com.demo.order.service.entity.Order;
import com.demo.order.service.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    Logger log = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplate restTemplate;

    public TransactionResponse saveOrder(TransactionRequest request) {
        String response = "";

        Order order = request.getOrder();
        orderRepository.save(order);

        Payment payment = request.getPayment();
        payment.setOrderId(order.getId());
        payment.setAmount(order.getPrice());

        log.info("order {}", order);
        log.info("payment {}", payment);

        // do rest call
        Payment paymentResponse = restTemplate.postForObject("http://PAYMENT-SERVICE/api/payment", payment, Payment.class);

        response = paymentResponse.getPaymentStatus().equals("success") ? "payment processing succesful and order places" : "there is a failure on payment api, order add to cart";



        return new TransactionResponse(order, paymentResponse.getAmount(), paymentResponse.getTransactionId(), response);
    }

//    public TransactionResponse getOrderByIdAndPayment(String orderId) {
//        Order order = orderRepository.getOne(orderId);
//        String url = "http://localhost:9092/api/payment/order/" + orderId;
//        Payment payment = restTemplate.getForObject(url, Payment.class);
//
//        log.info("payment {}", payment);
//
//        return new TransactionResponse(order, payment.getAmount(), payment.getTransactionId(), "success");
//    }

    public List<Order> getAllOrder() {
        return (List<Order>) orderRepository.findAll();
    }

    public String deleteOrder() {
        orderRepository.deleteAll();

        return "success delete order";
    }
}
