package com.demo.order.service.controller;

import com.demo.order.service.common.Payment;
import com.demo.order.service.common.TransactionRequest;
import com.demo.order.service.common.TransactionResponse;
import com.demo.order.service.entity.Order;
import com.demo.order.service.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    private OrderService service;

    @PostMapping("/order")
    public TransactionResponse bookOrder(@RequestBody TransactionRequest request) {
        return service.saveOrder(request);
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello World";
    }

    @GetMapping("/order")
    public List<Order> getAllOrder() {
        return service.getAllOrder();
    }

    @DeleteMapping("/order/delete")
    public String deleteOrder() {
        return service.deleteOrder();
    }
}
