package com.demo.order.service.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

    private String id;

    private String paymentStatus;

    private String transactionId;

    private String orderId;

    private double amount;

}
